#  Neural network inverted pendulum controller in OpenAI's Gym #

My [last go at this](https://www.praecipio.com/blog/praecipio-labs-control-systems-with-neural-networks) recreated the strategy that I learned in the early 2000's in college. For this next iteration I wanted to have something a little more real time to simulate controlling a physical robot, rather than build a model in the ether and animate the results. As promised I approached it using OpenAI's Gym environment. Gym is a great playground to tackle classic problems without having to write physics engines etc, and work in a standardized environment that other researchers (not that I qualify as a researcher) work in.

The inverted pendulum is not only a classic control problem but is also the basis for a [number](https://www.blueorigin.com/) of neat [projects](https://www.spacex.com/) out there. In gym each episode starts with the pendulum in a random position at the center of the track. The goal is to keep the pendulum upright for 200 steps (really its a 195 step average over 100 episodes). Each "step" is a tick of a virtual clock during which gravity is pulling the arm down, and you can move the shuttle left or right to counteract that. At each step you move the cart, and the new position of the pendulum is calculated. That new state is fed back to your controller to determine if it should move left or right in the next step.

## Approach ##

I tried several approaches, and eventually became obsessed with being on the OpenAI leader board for solving this problem so the [linked code](https://bitbucket.org/cpepe/nonlinear-control/src/5209d077b63d62785b8413da045b50018876dd70/inverted-pendulum/nn-inverted-pend.py?at=master&fileviewer=file-view-default) is more complex than it needs to be. (FWIW my solution regularly performs well enough to make the leaderboard but doesn't do it every time so I don't know that that counts.) The initial solution was the most elegant and the more I bolted on the more complex my solution was while not really achieving any performance gains. There's a lesson for you.

What I found was that a neural network could start with no knowledge of its world, and sometimes eventually learn to control the system. I never found that je ne sais quoi to make it repeatable. It would fail to find a way to improve and many generations would simply wallow in mediocrity. Allowing the controller to make random guesses while the neural network watched was much more effective. A purely random actor would consistently achieve 20 steps before failing, but rarely went more than 40-60 steps. A neural network actor that took a random action a small percentage of the time, and retrained itself on its best episodes most quickly, and successfully achieved the goal of keeping the arm upright for 200 steps.

## But what was the approach? ##

The solution that worked the best was to allow a random actor to balance the pole for several episodes and keep track of the best ones. The neural network is initially trained on the best random action episodes (where current state in the input to the network and next action is the output). Then the neural network actor is allowed to control the episode, however, 10-20% of the time the neural network actor will still make a random choice. Again the best episodes are tracked, and if the result is as good as, or better, than the current average then the network is retrained on the new episodes. In this way only the highest scores are used to retrain the network and it improves over time. (This method still routinely produced generations that would fail to improve and in some trials got worse, but the majority of attempts yielded success).

## Results ##

In this video the first part is the neural network actor learning to control the cart over time. An interesting observation is that since the controller only needs to make it to 200 steps it never achieves a circus clown level of skill. The second half of the video shows a neural network actor that was trained to balance the pole for 1000 steps. Its worth noting the difference. Its also worth noting that you should also overtrain your skills so that when you need to perform its not the hardest thing you have to do that day.

https://www.youtube.com/watch?v=ycsYhmwX9lM

## A bunch of data ##

This is the terminal output of the program. It starts off by playing several episodes with a random actor and tracking the score. On this run the random actor achieved an average of 22 steps per episode before failing. However, there are a few that are >40 steps and those were used to perform the initial training of the neural network actor. It then played episodes and retrained itself when it performed well (most learning initially came from random actions that the neural actor took during the episode).

You can see that it quickly converged on the solution in this run with each neural network actor episode scoring 200 (the initial 50 in the score list is a programming shortcut that I took that I'd do well to remove).

The accuracy bounces around a bit. I found that if the accuracy was >90% the actor would always win the episode, and despite it starting at 94% it trained down to 89.95%. This would be a good area to look at improving (if retraining didn't yield better accuracy then ignore it). Still this was good enough and the neural network actor got a perfect score thus solving the challenge.

In this case the random actor played 10 episodes, and the neural network actor played 525 episodes before being confident enough to try the test (score 195 on 100 consecutive episodes). So at 535 episodes that still puts this solution on the [leaderboard](https://github.com/openai/gym/wiki/Leaderboard). Again, 535 is not consistent. Sometimes it's less, sometimes it's more.

```
Done training trials [21.982857142857142] : [12.0, 12.0, 16.0, 18.0, 23.0, 11.0, 15.0, 27.0, 16.0, 19.0, 19.0, 12.0, 25.0, 21.0, 18.0, 23.0, 24.0, 14.0, 22.0, 16.0, 27.0, 18.0, 15.0, 14.0, 14.0, 15.0, 14.0, 21.0, 23.0, 26.0, 25.0, 17.0, 10.0, 12.0, 19.0, 46.0, 12.0, 28.0, 22.0, 19.0, 13.0, 24.0, 28.0, 12.0, 28.0, 12.0, 16.0, 21.0, 20.0, 33.0, 16.0, 18.0, 15.0, 19.0, 20.0, 18.0, 14.0, 13.0, 15.0, 24.0, 21.0, 38.0, 28.0, 62.0, 15.0, 28.0, 14.0, 16.0, 45.0, 13.0, 16.0, 15.0, 14.0, 12.0, 18.0, 27.0, 11.0, 54.0, 29.0, 26.0, 17.0, 22.0, 13.0, 25.0, 8.0, 22.0, 21.0, 10.0, 20.0, 46.0, 13.0, 13.0, 12.0, 21.0, 25.0, 15.0, 22.0, 29.0, 35.0, 33.0, 21.0, 25.0, 41.0, 37.0, 34.0, 18.0, 13.0, 34.0, 21.0, 13.0, 11.0, 21.0, 21.0, 26.0, 10.0, 31.0, 23.0, 57.0, 54.0, 35.0, 20.0, 17.0, 30.0, 9.0, 18.0, 20.0, 15.0, 16.0, 21.0, 40.0, 26.0, 16.0, 26.0, 15.0, 24.0, 16.0, 14.0, 13.0, 18.0, 14.0, 21.0, 11.0, 14.0, 10.0, 37.0, 20.0, 22.0, 13.0, 30.0, 39.0, 26.0, 48.0, 23.0, 15.0, 15.0, 16.0, 11.0, 49.0, 29.0, 26.0, 34.0, 14.0, 26.0, 22.0, 25.0, 13.0, 15.0, 15.0, 12.0, 12.0, 37.0, 18.0, 12.0, 29.0, 19.0, 43.0, 25.0, 34.0, 20.0, 13.0, 13.0, 11.0, 23.0, 15.0, 22.0, 40.0, 16.0, 15.0, 30.0, 62.0, 22.0, 10.0, 20.0, 10.0, 32.0, 49.0, 44.0, 14.0, 12.0, 18.0, 14.0, 10.0, 18.0, 30.0, 23.0, 33.0, 20.0, 40.0, 9.0, 38.0, 16.0, 21.0, 19.0, 16.0, 50.0, 25.0, 28.0, 16.0, 11.0, 17.0, 19.0, 10.0, 21.0, 22.0, 19.0, 16.0, 42.0, 17.0, 15.0, 31.0, 38.0, 14.0, 16.0, 16.0, 20.0, 23.0, 15.0, 23.0, 42.0, 19.0, 9.0, 14.0, 13.0, 20.0, 11.0, 10.0, 20.0, 12.0, 19.0, 18.0, 11.0, 16.0, 19.0, 27.0, 15.0, 26.0, 14.0, 45.0, 11.0, 13.0, 11.0, 16.0, 12.0, 29.0, 30.0, 10.0, 15.0, 12.0, 17.0, 13.0, 27.0, 31.0, 22.0, 32.0, 11.0, 17.0, 25.0, 30.0, 12.0, 16.0, 11.0, 13.0, 13.0, 46.0, 53.0, 21.0, 16.0, 17.0, 21.0, 18.0, 17.0, 13.0, 17.0, 15.0, 15.0, 21.0, 25.0, 33.0, 15.0, 13.0, 31.0, 10.0, 19.0, 16.0, 12.0, 14.0, 15.0, 62.0, 20.0, 18.0, 24.0, 17.0, 18.0, 28.0, 35.0, 13.0, 30.0, 21.0, 11.0, 27.0, 19.0, 43.0, 31.0, 18.0, 24.0, 14.0, 19.0, 12.0, 18.0, 39.0, 13.0, 10.0, 27.0, 27.0, 13.0, 10.0, 14.0, 23.0, 9.0, 24.0, 24.0, 22.0, 20.0, 11.0, 19.0, 15.0, 22.0, 15.0, 22.0, 13.0, 13.0, 25.0, 13.0, 32.0, 24.0, 29.0, 23.0, 18.0, 20.0, 34.0, 34.0, 14.0, 26.0, 22.0, 17.0, 14.0, 37.0, 17.0, 17.0, 18.0, 10.0, 20.0, 22.0, 45.0, 20.0, 28.0, 16.0, 18.0, 28.0, 16.0, 19.0, 21.0, 15.0, 32.0, 20.0, 13.0, 17.0, 8.0, 14.0, 13.0, 11.0, 34.0, 42.0, 18.0, 20.0, 28.0, 27.0, 50.0, 10.0, 19.0, 15.0, 22.0, 14.0, 16.0, 27.0, 17.0, 35.0, 25.0, 24.0, 12.0, 12.0, 14.0, 11.0, 15.0, 15.0, 14.0, 27.0, 34.0, 51.0, 21.0, 28.0, 16.0, 16.0, 27.0, 23.0, 15.0, 25.0, 39.0, 57.0, 38.0, 27.0, 11.0, 18.0, 11.0, 18.0, 58.0, 13.0, 19.0, 30.0, 10.0, 52.0, 39.0, 21.0, 14.0, 27.0, 23.0, 10.0, 21.0, 42.0, 27.0, 15.0, 40.0, 12.0, 23.0, 24.0, 29.0, 23.0, 16.0, 27.0, 31.0, 21.0, 13.0, 12.0, 19.0, 18.0, 27.0, 15.0, 34.0, 19.0, 17.0, 16.0, 25.0, 25.0, 9.0, 10.0, 11.0, 12.0, 21.0, 25.0, 27.0, 12.0, 22.0, 16.0, 33.0, 16.0, 15.0, 28.0, 25.0, 20.0, 25.0, 23.0, 17.0, 22.0, 65.0, 20.0, 16.0, 25.0, 26.0, 27.0, 50.0, 25.0, 50.0, 15.0, 45.0, 55.0, 13.0, 23.0, 36.0, 19.0, 17.0, 10.0, 12.0, 26.0, 20.0, 25.0, 39.0, 17.0, 33.0, 16.0, 14.0, 33.0, 22.0, 12.0, 31.0, 62.0]
Retraining [162.5] : [50, 200.0, 200.0, 200.0]
...
Retraining [185.0] : [50, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0]
...
Epoch 1/20
199/199 [==============================] - 0s 31us/step - loss: 0.3300 - acc: 0.9397
Epoch 2/20
199/199 [==============================] - 0s 24us/step - loss: 0.3299 - acc: 0.9397
Epoch 3/20
199/199 [==============================] - 0s 23us/step - loss: 0.3163 - acc: 0.9246
Epoch 4/20
199/199 [==============================] - 0s 26us/step - loss: 0.3074 - acc: 0.8894
Epoch 5/20
199/199 [==============================] - 0s 24us/step - loss: 0.3146 - acc: 0.8442
Epoch 6/20
199/199 [==============================] - 0s 24us/step - loss: 0.3191 - acc: 0.8442
Epoch 7/20
199/199 [==============================] - 0s 24us/step - loss: 0.3122 - acc: 0.8442
Epoch 8/20
199/199 [==============================] - 0s 27us/step - loss: 0.3085 - acc: 0.8894
Epoch 9/20
199/199 [==============================] - 0s 25us/step - loss: 0.3055 - acc: 0.9146
Epoch 10/20
199/199 [==============================] - 0s 30us/step - loss: 0.3100 - acc: 0.9196
Epoch 11/20
199/199 [==============================] - 0s 26us/step - loss: 0.3091 - acc: 0.9146
Epoch 12/20
199/199 [==============================] - 0s 23us/step - loss: 0.3043 - acc: 0.9146
Epoch 13/20
199/199 [==============================] - 0s 24us/step - loss: 0.3024 - acc: 0.8894
Epoch 14/20
199/199 [==============================] - 0s 24us/step - loss: 0.3017 - acc: 0.8794
Epoch 15/20
199/199 [==============================] - 0s 23us/step - loss: 0.3046 - acc: 0.8643
Epoch 16/20
199/199 [==============================] - 0s 23us/step - loss: 0.3041 - acc: 0.8543
Epoch 17/20
199/199 [==============================] - 0s 30us/step - loss: 0.3019 - acc: 0.8744
Epoch 18/20
199/199 [==============================] - 0s 25us/step - loss: 0.2980 - acc: 0.8945
Epoch 19/20
199/199 [==============================] - 0s 28us/step - loss: 0.3009 - acc: 0.8995
Epoch 20/20
199/199 [==============================] - 0s 26us/step - loss: 0.2983 - acc: 0.9196
...
#Final Accuracy
199/199 [==============================] - 0s 26us/step - loss: 0.3503 - acc: 0.8995
...
Details: [200.0 : 200.0] [200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0, 200.0]

Score: 200.0
NN Episodes: 10
Training Episodes: 525
```

## References ##

This has been solved many times. Some good articles include these:

  *  https://medium.com/@tuzzer/cart-pole-balancing-with-q-learning-b54c6068d947
  *  https://medium.com/@rajmandloi/genetic-algorithm-with-neural-network-cc9961221e42
