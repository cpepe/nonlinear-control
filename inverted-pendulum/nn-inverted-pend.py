#reference article https://pythonprogramming.net/openai-cartpole-neural-network-example-machine-learning-tutorial/

import gym
import numpy as np
from keras.utils.np_utils import to_categorical
from keras.models import Sequential
from keras.layers import Dense, Activation

#how many episodes should the neural network play and learn from
nnLearningEpisodesToPlay = 125
'''
These params control how other actors can seed the nn with training data

trainingDataEpisodesToPlay - how many episodes should another actor play to seed the NN with training data
seedWithRandomMoves - if True run trainingDataEpisodesToPlay episodes to discover some successful approaches and train the nn with that before having it play and learn
minReward - minimum threshold for the reward from a trainingDataEpisodesToPlay episode to be used for training the neural network
'''
trainingDataEpisodesToPlay = 100
seedWithRandomMoves = False
minReward = 50
GOAL = 200.0 #200 is hardcoded somewhere in the env it seems, bump to 1000 for super training

'''
showWorld controls whether or not to render the game screen. It is much faster to
not show what the cart is doing so training in the background and occasionally
showing it is a good balance of speed and joy.

NOTE: this is toggled below
'''
showWorld = False
#nn hyperparams
epochs=20
batch_size=64
'''
loadModel - None or path to file to load model from saved file instead of
  learning from scratch

e.g. loadModel = 'expert_inv_pend.h5'
'''
loadModel = None

#probability of doing something random in leanInActor and nnActor
#the randomness seems to help the nn learn new techniques by trying things it
#otherwise wouldn't
randActionProbability = 0.15

#this allows one to customize the gym environment to override the hard limits on
#steps and or reward max (this env is considered solved at 200 steps/reward)
gym.envs.register(
    id='CartPole-customgoal-v0',
    entry_point='gym.envs.classic_control:CartPoleEnv',
    max_episode_steps=GOAL,
    reward_threshold=-GOAL,
)

env = gym.make('CartPole-customgoal-v0')
train_obs = list()
train_labels = list()

if loadModel is not None:
    from keras.models import load_model
    model = load_model(loadModel)
else:
    model = Sequential([
        Dense(256, input_shape=(4,)),
        Activation('relu'),
        Dense(256),
        Activation('relu'),
        Dense(256),
        Activation('relu'),
        Dense(2),
        Activation('softmax'),
    ])
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    print(model.summary())

def trainNNActor(data, labels):
    data = np.array(data)
    #convert lables to one hot encoding
    categorical_labels = to_categorical(labels, num_classes=2)
    model.fit(data, categorical_labels, epochs=epochs, batch_size=batch_size)
    model.save('trained_inv_pend.h5')
'''
Actors are the agents that decide what action to take given an observation
nnActor - use the neural network to decide
randomActor - sample a random action from the action_space
leanInActor - move in the direction that the rod is leaning
'''
def nnActor(state):
    #inject randomness to allow the network to continue to learn
    #by discovering new solutions and forcing it into unknown spaces
    if np.random.random() > (1 - randActionProbability):
        return env.action_space.sample()
    #network predicts in one hot format so convert back to action
    #make array of 1 array
    state = np.expand_dims(state, axis=0)
    hot_action = model.predict(state, batch_size=None, verbose=1, steps=1 )
    action = np.argmax(hot_action, axis=1)
    return action[0]

def randomActor(state):
    return env.action_space.sample()

def leanInActor(state):
    '''
    move toward the direction the pole is leaning
    experiments show that this scores a bit better than 40 on average (43ish)

    10% of the time make a random move instead - attempt to inject some ability
    to discover better ways to balance
    '''
    if np.random.random() > (1 - randActionProbability):
        return env.action_space.sample()

    if state[2] > 0:
        return 1
    else:
        return 0

def episode(decisionEngine):
    #action: 0 - right, 1 - left
    #obs: [position of cart, velocity of cart, angle of pole, rotation rate of pole]
    obs = env.reset()
    done = False
    reward = 0
    #this can be training data. For a given observation track the action
    #that was taken. If it was successful then use it as training data
    observations = list() #list of states
    moves = list() #given a state the move that was made
    while not done and reward < GOAL:
        if showWorld:
            env.render()
        action = decisionEngine(obs)
        obs,  rwd,  done,  info = env.step(action)
        reward += rwd
        observations.append(obs)
        moves.append(action)
    #delete the first action and last observation to align obs and the action
    #that was taken
    del observations[-1]
    del moves[0]

    return reward, observations, moves

if __name__ == "__main__":
    r_mean = list()
    if seedWithRandomMoves:
        #get some training data
        for _ in range(0,trainingDataEpisodesToPlay):
            r,o,m = episode(randomActor)
            if r > minReward:
                #since we consider state:action independent I just need a
                #single list of observations and actions, episode is
                #not important
                train_obs.extend(o)
                train_labels.extend(m)
            r_mean.append(r)
        print('Done trials [%s] : %s' % (np.mean(r_mean), r_mean))
        trainNNActor(train_obs, train_labels)
    #from here on the neural network will do the controlling and learn from itself
    nn_rewards = list()
    for _ in range(0,5):
        r,o,m = episode(nnActor)
        nn_rewards.append(r)
    print('Mean reward from nn actor: [%s] %s' % (np.mean(nn_rewards), nn_rewards))

    #keep playing and training with the nn
    train_obs = list()
    train_labels = list()
    for i in range(0,nnLearningEpisodesToPlay):
        #watch an episode play out now and then
        if i%30==0:
            showWorld = True
        else:
            showWorld = False
        r,o,m = episode(nnActor)
        #if we do pretty good then use that for training data
        if r >= np.max(nn_rewards)*0.8:
            train_obs.extend(o)
            train_labels.extend(m)
        nn_rewards.append(r)
        if i % 5 == 0 and len(train_obs) > 0:
            print('Retraining [%s] : %s' % (np.mean(nn_rewards), nn_rewards))
            trainNNActor(train_obs, train_labels)
            train_obs = list()
            train_labels = list()
        else:
            print('Not good enough to retrain: [%s : %s] %s' % (np.max(nn_rewards), np.mean(nn_rewards), nn_rewards))
