import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

fig, ax = plt.subplots()
points, = ax.plot([0,], [0,], marker='^', linestyle='None')
plt.pause(15)
#epochs = [1,2,4,12,20,100]
epochs = [16,18,19,20,50,100]
for epoch in epochs:
    df = pd.read_csv('dragon-%s.csv' % (epoch))
    y = df['pos']
    x = [20 for x in range(0,len(y))]
    for t in range( len(y) ):
        if t == 0:
            points, = ax.plot(x, y, marker='^', linestyle='None')
            ax.set_xlim(0, 40)
            #ax.set_ylim(0, y.max())
            ax.set_ylim(0, 70)
            plt.pause(0.5)
        else:
            ax.legend(['Epochs: %s\n[y=%0.2f]' % (epoch, y[t])])
            new_y = y[t]
            points.set_data(x, new_y)
        plt.pause(0.05)
