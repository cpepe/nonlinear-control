# control a rocket and cause it to hover in place
# F = ma, a = [-gravity, thrust/mass]
# deltaH = a*t^2 + v*t
# deltaV = 2*a*t + v

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from keras.models import Sequential
from keras.layers import Dense, Activation, LSTM, Flatten
from sklearn import preprocessing


#World
gravity = -9.8 #m/s^2
initialHeight = 15
initialSpeedY = 0
controlFrequency = 2.0 #Hz
timeStep = 1/controlFrequency #seconds
initialMass = 1000.0 #kg, keep floating point
thrust_max = 10000 #N
thrust_min = 9000 #N
#travel info
travel_time = 45.0
train = True
useRnn = False

class Rocket():
    def __init__(self):
        self.time = 0
        self.x = 22
        self.y = initialHeight
        self.goal = 50
        self.mass = initialMass
        self.thrust = 0 #Newtons
        self.speedY = initialSpeedY
        self.accelerationY = gravity
        #controller details
        self.controller = self.getNNModel()

    def setThrust(self, thrust):
        self.thrust = thrust

    def getThrustFromTotalAcceleration(self, row):
        '''
        determine the thrust needed to move a given displacement given current
        state. This is the updateState formulas solved for thrust instead of
        position. Used in generating training data.
        '''
        #df = [time,pos,deltaPos,v,a]
        mass = self.mass #only allowing for static mass, might could add to df
        accelY = row['a']

        #calculate the thrust needed to achieve a given acceleration based on thrust assuming
        #it is instantaneously applied
        ##thrust = mass*(accelY + newAccelY + gravity)
        totalAcc = accelY - gravity
        return max(0,mass*totalAcc) #limit thrust to a single downward engine

    def updateState(self):
        self.time += timeStep
        #calculate the current acceleration based on thrust assuming
        #it is instantaneously applied
        self.accelerationY = (self.thrust/self.mass) + gravity
        #calculate the speed based on acceleration assuming
        #it has instantaneously changes and is constant
        # (we could += here but it makes the physics harder to follow)
        self.speedY = 2*self.accelerationY*timeStep + self.speedY
        #calculate the change in height given our current state assuming
        #everything is constant over the time step
        ##NOTE: oddly self.accelerationY**timeStep creates an imaginary number
        newY = self.accelerationY*timeStep*timeStep + self.speedY*timeStep + self.y
        #the ground is 0
        if newY >=0:
            self.y = newY
        else:
            self.y = 0
            self.speedY = 0

    def stateToCSVLine(self):
        '''return a line of all of the rocket metrics at a given state suitable for a CSV file'''
        return '%s,%s,%s,%s,%s,%s,%s\n' % (self.time,self.x,self.y,self.mass,self.thrust,self.speedY,self.accelerationY)

    def generateTrajectory(self, timeToGoal):
        return self.mjtg(self.y, self.goal, controlFrequency, timeToGoal)

    def mjtg(self, current, setpoint, frequency, move_time):
        '''this method plans a trajectory based on the start and end conditions
            using constant jerk to define the path

            source https://mika-s.github.io/python/control-theory/trajectory-generation/2017/12/06/trajectory-generation-with-a-minimum-jerk-trajectory.html
        '''
        df = list()
        #original code has this wrapped in int() but that causes rounding error add
        #position always = 0. Moved int() to range() in for loop
        timefreq = move_time * frequency
        print("time steps = %s" % (timefreq))
        for time in range(1, int(timefreq)):
            pos = current + (setpoint - current) * (10.0 * (time/timefreq)**3 - 15.0 * (time/timefreq)**4 + 6.0 * (time/timefreq)**5)
            v = frequency * (setpoint - current) * (30.0 * (time)**2.0 * (1.0/timefreq)**3 - 60.0 * (time)**3.0 * (1.0/timefreq)**4 + 30.0 * (time)**4.0 * (1.0/timefreq)**5)
            if time > 1:
                lastpos = current + (setpoint - current) * (10.0 * ((time-1)/timefreq)**3 - 15.0 * ((time-1)/timefreq)**4 + 6.0 * ((time-1)/timefreq)**5)
                deltaPos = pos-lastpos
            else:
                deltaPos = 0
            deltaGoal = self.goal-pos
            timeLeft = move_time - time/timefreq
            df.append( [time, timeLeft, pos, deltaPos, deltaGoal, v] )
        df = pd.DataFrame(df, columns=['time', 'timeLeft', 'pos', 'deltaPos', 'deltaGoal', 'v'])
        #estimate the acceleration in the traj profile
        a = np.gradient(df['v'])
        df['a'] = a
        #given df determine the required thrust for each step and updated df to
        #create a complete training dataset
        df['thrust'] = df.apply(self.getThrustFromTotalAcceleration, axis=1)
        return df

    def exampleTrajectory():
        # Set up and calculate trajectory.
        average_velocity = 10.0
        current = 0.0
        setpoint = 100.0
        frequency = 1000.0 #originally int, needs to be float
        steps = (setpoint - current) / average_velocity

        df = self.mjtg(current, setpoint, frequency, steps)

        # Create plot.
        xaxis = [i / frequency for i in range(1, int(steps * frequency))]
        plt.plot(xaxis, df['pos'])
        plt.plot(xaxis, df['v'])
        plt.title("Minimum jerk trajectory")
        plt.xlabel("Time [s]")
        plt.ylabel("Position and velocity [m/s]")
        plt.legend(['pos', 'vel'])
        plt.show()

    def getDenseModel(self):
        #2 ltsm layers, single dense out using sigmoid
        #inputs: time,y,goal,speed,acceleration
        #outputs: thrust
        model = Sequential([
            Dense(128, input_shape=(3,)),
            Activation('relu'),
            Dense(32),
            Activation('relu'),
            Dense(1),
            Activation('sigmoid'),
        ])
        return model

    def getRNNModel(self):
        model = Sequential([
            LSTM(128, input_shape=(89,3)),
            Activation('relu'),
            Dense(1),
            Activation('sigmoid'),
        ])
        return model

    def getNNModel(self):
        if useRnn:
            model = self.getRNNModel()
        else:
            model = self.getDenseModel()
        model.compile(optimizer='adam', loss='mse', metrics=['accuracy'])
        print(model.summary())
        return model

    def fitController(self, data, labels):
        # Train the model, iterating on the data in batches of 128 samples
        if useRnn:
            samples = len(data)
            data = np.array( data )
            data = data.reshape(2,89,3)
            labels = labels.reshape(2,89,1)
            print(data.shape)
        self.controller.fit(data, labels, epochs=19, batch_size=64)
        self.controller.save('trained_rocket.h5')

def normalizeThrust(v):
    '''
    simple normalization to scale predict inputs
    '''
    return (v-thrust_min)/(thrust_max-thrust_min)

def normalize(v,mn,mx):
    '''
    simple normalization to scale predict inputs
    '''
    return (v-mn)/(mx-mn)

def hugeify(n,mn,mx):
    '''
    simple method to return normalized outputs to scaled value
    '''
    return n*(mx-mn)+mn

def generateNormalizedData():
    df = r.generateTrajectory(travel_time)
    df.to_csv('trajectory.csv')
    #get normalization max/mins
    #prep the training data
    #normalize, then carve out data and labels
    min_max_scaler = preprocessing.MinMaxScaler()
    np_scaled = min_max_scaler.fit_transform(df)
    df_normalized = pd.DataFrame(np_scaled, columns=df.columns)
    data = df_normalized[['timeLeft', 'pos', 'deltaGoal']]
    labels = df['thrust']
    labels = np.array([normalizeThrust(l) for l in labels])
    print(labels)
    return data,labels

if __name__ == "__main__":
    if train == True:
        r = Rocket()
        d1,l1 = generateNormalizedData()
        if useRnn:
            r.goal = r.goal*1.5 #make two trajectories to train with
            d2,l2 = generateNormalizedData()
            print('1=%s, 2=%s' % (len(d1), len(d2)))
            data = pd.concat([d1,d2])
            labels = np.array([l1,l2])
        else:
            data = d1
            labels = l1
        #fit here
        r.fitController(data, labels)

    from keras.models import load_model # To save and load model
    # Load the model into new rocket
    dragon = Rocket()
    dragon.controller = load_model('trained_rocket.h5')
    #try to hit the goal using the physics model and nn controller
    timefreq = travel_time * controlFrequency
    print("test run time steps = %s" % (timefreq))
    dragonFrame = list()
    with open('dragon.csv', 'w') as f:
        for time in range(1, int(timefreq)):
            #inputs: ['timeLeft', 'pos', 'deltaGoal']
            tl = normalize(travel_time-dragon.time, 0, travel_time)
            pos = normalize(dragon.y, 0, dragon.goal) #goal isn't the possible max but is in the training data
            dg = normalize(dragon.goal-dragon.y, 0, dragon.goal)
            state = [tl, pos, dg]
            state = np.array( [state,] )
            thrust_normal = dragon.controller.predict( state, batch_size=None, verbose=1, steps=1 )
            dragon.thrust = hugeify(float(thrust_normal), thrust_min, thrust_max )
            dragonFrame.append( [travel_time-dragon.time, dragon.y, dragon.goal-dragon.y, dragon.thrust] )
            dragon.updateState()
            f.write(dragon.stateToCSVLine())
    dragonFrame = pd.DataFrame(dragonFrame, columns=['timeLeft', 'pos', 'deltaGoal', 'thrust'])
    dragonFrame.to_csv('dragonFrame.csv')
